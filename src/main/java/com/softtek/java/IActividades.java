package com.softtek.java;

public interface IActividades {

    void actividadActual();

    default void diseñoVistaWeb() {
        System.out.println("Diseñando....");
    }

    default void mantenimientoAplicacion() {
        System.out.println("Mantenimiento....");
    }

    default void resolverTickets() {
        System.out.println("Resolviendo tickets....");
    }
}
