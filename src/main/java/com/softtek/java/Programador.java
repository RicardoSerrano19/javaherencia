package com.softtek.java;

import java.util.List;

public class Programador extends Empleado {
    public List<String> lenguajes;
    public List<String> frameworks;
    public String tituloProfesional;

    public void getLenguajes() {
        lenguajes.stream()
                .forEach(System.out::println);
    }

    public void getFrameworks() {
        frameworks.stream()
                .forEach(System.out::println);
    }

    @Override
    public String toString() {
        return "Titulo profesional: " + tituloProfesional;
    }
}
