package com.softtek.java;

import java.util.List;

public class IngenieroSoporte extends Empleado implements IActividades{
    public List<String> tecnologiasSoporte;

    public void getTecnologias(){
        tecnologiasSoporte.stream()
                .forEach(System.out::println);
    }

    @Override
    public void actividadActual() {
        resolverTickets();
    }
}
