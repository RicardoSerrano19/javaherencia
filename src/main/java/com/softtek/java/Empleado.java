package com.softtek.java;

public class Empleado {
    private int id;
    public int age;
    public String name;
    private String address;
    private int salary;
    private int bono;

    public Empleado(){
        id = (int)(Math.random() * ((50000 - 10000) + 1) + 10000);
        salary = (int)(Math.random() * ((80000 - 10000) + 1) + 10000);
        bono = salary > 50000 ? 10000 : 5000;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getBono() {
        return bono;
    }

    public void setBono(int bono) {
        this.bono = bono;
    }
}
