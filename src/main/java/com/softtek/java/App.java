package com.softtek.java;

import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.Arrays;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //Se puede asignar un objeto de tipo empleado a un objeto de tipo programador?
        //Programador programador = new Empleado(); //-> No

        //Se puede asignar un objeto de tipo empleado a un objeto de tipo programador web?
        //ProgramadorWeb programadorWeb = new Empleado(); //-> No

        //Se puede asignar un objeto de tipo empleado a un objeto de tipo programador mobile?
        //ProgramadorMobile programadorMobile = new Empleado(); //-> No

        //Se puede asignar un objeto de tipo empleado a un objeto de tipo ingeniero soporte?
        //IngenieroSoporte ingenieroSoporte = new Empleado(); //->No

        //Se puede asignar un objeto de tipo programador a un objeto de tipo empleado?
        Empleado empleado1 = new Programador(); //-> Si

        //Se puede asignar un objeto de tipo programador web a un objeto de tipo empleado?
        Empleado empleado2 = new ProgramadorWeb(); //-> Si

        //Se puede asignar un objeto de tipo programador mobile  a un objeto de tipo empleado?
        Empleado empleado3 = new ProgramadorMobile(); //-> Si

        //Se puede asignar un objeto de tipo ingeniero soporte a un objeto de tipo empleado?
        Empleado empleado4 = new IngenieroSoporte(); // -> Si

        //se puede asignar un objeto de tipo programador un objeto de tipo programador web?
        //ProgramadorWeb programadorWeb = new Programador(); // -> No

        //se puede asignar un objeto de tipo programador un objeto de tipo programador mobile?
        //ProgramadorMobile programadorMobile = new Programador(); // -> No

        //se puede asignar un objeto de tipo programador web un objeto de tipo programador?
        Programador programador1 = new ProgramadorWeb(); //- SI

        //se puede asignar un objeto de tipo programador mobile un objeto de tipo programador?
        Programador programador2 = new ProgramadorMobile(); // -> SI

        Empleado nuevoProgramadorWeb = new ProgramadorWeb();
        System.out.println("Salario del nuevo empleado: $" + nuevoProgramadorWeb.getSalary() +
                            " Y el bono: $" + nuevoProgramadorWeb.getBono());
        ((ProgramadorWeb) nuevoProgramadorWeb).actividadActual();
        ((ProgramadorWeb) nuevoProgramadorWeb).tecnologiasWeb = Arrays.asList("Bootstrap","CSS3", "SVG");
        ((ProgramadorWeb) nuevoProgramadorWeb).getTecnologias();

        System.out.println("-----------------");
        Empleado ingenieroSoporte = new IngenieroSoporte();
        System.out.println("ID del Ing. Soporte: " + ingenieroSoporte.getId());
        ((IngenieroSoporte) ingenieroSoporte).tecnologiasSoporte = Arrays.asList("Office");
        ((IngenieroSoporte) ingenieroSoporte).getTecnologias();
        System.out.println("-----------------");
        Empleado empleadoProgramador = new Programador();
        ((Programador) empleadoProgramador).tituloProfesional = "Doctorado";
        System.out.println(((Programador) empleadoProgramador).tituloProfesional);


    }
}
