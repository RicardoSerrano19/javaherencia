package com.softtek.java;

import java.util.List;

public class ProgramadorMobile extends Programador implements IActividades{
    public List<String> tecnologiasMoviles;
    public Integer experiencia;

    public ProgramadorMobile(){
        experiencia = (int)(Math.random() * ((15 - 1) + 1) + 1);
    }

    public void getTecnologias(){
        tecnologiasMoviles.stream()
                .forEach(System.out::println);
    }

    @Override
    public void actividadActual() {
        mantenimientoAplicacion();
    }
}
