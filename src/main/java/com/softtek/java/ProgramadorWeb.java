package com.softtek.java;

import java.util.List;

public class ProgramadorWeb extends Programador implements IActividades{
    public List<String> tecnologiasWeb;
    public Integer experiencia;

    public ProgramadorWeb(){
        experiencia = (int)(Math.random() * ((15 - 1) + 1) + 1);
    }
    public void getTecnologias(){
        tecnologiasWeb.stream()
                .forEach(System.out::println);
    }

    @Override
    public void actividadActual() {
        diseñoVistaWeb();
    }
}
